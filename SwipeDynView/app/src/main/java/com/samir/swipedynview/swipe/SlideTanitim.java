package com.samir.swipedynview.swipe;

import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.samir.swipedynview.R;
import com.samir.swipedynview.Util;

public class SlideTanitim extends AppCompatActivity {
    private ViewPager mPager;
    private int[] layouts = {R.layout.slide_one, R.layout.slide_two,
            R.layout.slide_three};
    private MpagerAdapter mPagerAdapter;
    private LinearLayout dotsDisplayLayout,linlay_container;
    private ImageView[] dots;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Util.getInstance(SlideTanitim.this).getFullScreenTransparancy(SlideTanitim.this);

        setContentView(R.layout.act_slide);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        mPager = (ViewPager) findViewById(R.id.viewpager_act_slide);
        mPagerAdapter = new MpagerAdapter(layouts, this);
        mPager.setAdapter(mPagerAdapter);

        dotsDisplayLayout = (LinearLayout) findViewById(R.id.linlay_act_slide);
        linlay_container = (LinearLayout) findViewById(R.id.linlay_act_slide_contaner);
        createDots(0);
        setDynamicViews(params);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if (position == layouts.length) {


                } else {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void createDots(int current_position) {

        if (dotsDisplayLayout != null)
            dotsDisplayLayout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.dots_active));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.dots_inactive));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            dotsDisplayLayout.addView(dots[i], params);
        }
    }

    private void setDynamicViews(LinearLayout.LayoutParams params) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        final Button btn = new Button(this);
        // Give button an ID
        btn.setId(9+1);
        btn.setText("Add To Cart");
        btn.setLayoutParams(params);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Log.i("TAG", "index :" + 9);

                Toast.makeText(getApplicationContext(),
                        "Clicked Button Index :" + 10,
                        Toast.LENGTH_LONG).show();

            }
        });
        ll.addView(btn);
        linlay_container.addView(ll);
    }


    private void loadHome() {
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    private void loadNextSlide() {
        int nextSlide = mPager.getCurrentItem() + 1;
        if (nextSlide < layouts.length) {
            mPager.setCurrentItem(nextSlide);


        } else {
            loadHome();
            new PreferenceManager(this).writePreference();
        }

    }
}
