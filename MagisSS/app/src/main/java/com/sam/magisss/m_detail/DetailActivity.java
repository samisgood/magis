package com.sam.magisss.m_detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sam.magisss.R;
import com.sam.magisss.m_about.AboutAct;
import com.sam.magisss.m_new_list.NewListAct;
import com.sam.magisss.model.NewModel;
import com.sam.magisss.service.RModel;
import com.sam.magisss.service.APPService;
import com.sam.magisss.service.RModelDataItems;
import com.sam.magisss.service.ReadModel;
import com.sam.magisss.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailActivity extends AppCompatActivity {


    private ImageView img_back;
    private CardView mCv_about, mCv_new_list;
    private ListView mListView;
    private ProgressDialog pDialog;
    private Retrofit retrofit;

    private SharedPreferences shp;
    private SharedPreferences.Editor editor;
    private Retrofit restAdapter;
    private APPService restInterface;

    private String URL_OF_NEW;
    private String urlJsonObj = "https://api.androidhive.info/volley/person_object.json";
    private String jsonResponse;
    private static String TAG = DetailActivity.class.getSimpleName();
    private RModelDataItems currentNew;

    private TextView txt_title, txt_sub_title, txt_description;
    private ImageView img_;
    private int imageId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.getInstance(DetailActivity.this).getFullScreenTransparancy(DetailActivity.this);
        setContentView(R.layout.act_details);


        initialiSeFields();

        shp = getSharedPreferences("sam", MODE_PRIVATE);
        editor = shp.edit();
        currentNew = null;

        pDialog = new ProgressDialog(DetailActivity.this);
        pDialog.setMessage(getString(R.string.dialog_msg));
        pDialog.setCancelable(false);

        try {
            Gson gsonn = new Gson();
            String jsonn = shp.getString("news", "");
            NewModel modell = gsonn.fromJson(jsonn, NewModel.class);
            String samm = "";

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                imageId = extras.getInt("mage_id");
                //The key argument here must match that used in the other activity
            }


            if (modell != null) {
                URL_OF_NEW = modell.getImage();
                // getAndSetImageDetails(URL_OF_NEW, modell.getId());
                getAndSetImageDetails(URL_OF_NEW, imageId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        mCv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(DetailActivity.this, AboutAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        mCv_new_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(DetailActivity.this, NewListAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(DetailActivity.this, NewListAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

    }

    private void initialiSeFields() {
        img_back = (ImageView) findViewById(R.id.img_act_details_back);
        mCv_new_list = (CardView) findViewById(R.id.cardview_act_detail_new_list);
        mCv_about = (CardView) findViewById(R.id.cardview_act_detail_new_about);

        txt_title = (TextView) findViewById(R.id.txt_act_detals_title);
        txt_sub_title = (TextView) findViewById(R.id.txt_act_detals_sub_title);
        txt_description = (TextView) findViewById(R.id.txt_act_detals_description);
        img_ = (ImageView) findViewById(R.id.img_act_details_image);
    }

    private void setViewDetails(RModelDataItems modell) {
        if (currentNew != null) {
            txt_title.setText(modell.getTitle());
            txt_sub_title.setText(modell.getSubtitle());
            txt_description.setText(modell.getTitle());

            //****************************************************************
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar_act_details);
            ImageLoader.getInstance().displayImage(modell.getImage(), img_, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    img_.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.INVISIBLE);
                }
            });
            //****************************************************************


        }
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            try {
                pDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    private void isNewRead(int id) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo5362749.mockable.io/").
                        addConverterFactory(GsonConverterFactory.create())
                .build();
        APPService service = retrofit.create(APPService.class);

        final Call<ReadModel> reportMCall = service.saveReadNewDetails(id);
        reportMCall.enqueue(new Callback<ReadModel>() {
            @Override
            public void onResponse(Response<ReadModel> response, Retrofit retrofit) {
                hidepDialog();

                ReadModel datam = response.body();
                if (datam != null) {
                    boolean error = response.body().getError();

                    if (!error) {
                        Util.showMessage(DetailActivity.this, "New has benn read");
                    } else {
                        Util.showMessage(DetailActivity.this, "News Detail Error");
                        Log.d("onResponse", "" + response.code() +
                                "  response body " + response.body() +
                                " responseError " + response.errorBody() + " responseMessage " +
                                response.message());
                    }
                }


            }

            @Override
            public void onFailure(Throwable t) {
                hidepDialog();
                Util.showMessage(DetailActivity.this, "News Detail Connection Error");
                Log.d("onFailure", t.toString());
            }
        });


    }

    private void getAndSetImageDetails(String url, int id) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo5362749.mockable.io/").
                        addConverterFactory(GsonConverterFactory.create())
                .build();
        APPService service = retrofit.create(APPService.class);

        final Call<RModel> reportMCall = service.getNewsDetail(id);
        reportMCall.enqueue(new Callback<RModel>() {
            @Override
            public void onResponse(Response<RModel> response, Retrofit retrofit) {
                hidepDialog();

                RModel datam = response.body();
                if (datam != null) {
                    boolean status = response.body().isError();

                    if (!status) {
                        //  Util.showMessage(DetailActivity.this, "Oldu");

                        // todo adaptor
                        currentNew = datam.getResponseM().getNewDetails();
                        setViewDetails(currentNew);
                    } else {
                        Util.showMessage(DetailActivity.this, "News Detail Error");
                        Log.d("onResponse", "" + response.code() +
                                "  response body " + response.body() +
                                " responseError " + response.errorBody() + " responseMessage " +
                                response.message());
                    }
                }


            }

            @Override
            public void onFailure(Throwable t) {
                hidepDialog();
                Util.showMessage(DetailActivity.this, "News Detail Connection Error");
                Log.d("onFailure", t.toString());
            }
        });


    }


}
