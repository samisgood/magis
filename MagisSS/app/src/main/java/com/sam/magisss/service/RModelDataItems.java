package com.sam.magisss.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Samir on 29.4.2018.
 */
  /*
   "id": 1,
        "title": "Haber 1",
        "sub-title": "Haber 1 Alt Metin",
        "image": "https://images.pexels.com/photos/343219/pexels-photo-343219.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb",
        "writer": "Haber 1 Yazarı",
        "description":"Ha
     */
public class RModelDataItems implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("sub-title")
    @Expose
    private String subtitle;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("writer")
    @Expose
    private String writer;

    @SerializedName("description")
    @Expose
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }
}
