package com.sam.magisss.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sam.magisss.model.ResultData;

import java.io.Serializable;

/**
 * Created by Samir on 29.4.2018.
 */

public class RModel implements Serializable{


    @SerializedName("error")
    @Expose
    private boolean error;

    @SerializedName("response")
    @Expose
    private RModelData responseM;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public RModelData getResponseM() {
        return responseM;
    }

    public void setResponseM(RModelData responseM) {
        this.responseM = responseM;
    }
}
