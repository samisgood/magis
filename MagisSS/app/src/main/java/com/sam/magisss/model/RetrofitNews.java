package com.sam.magisss.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samir on 29.4.2018.
 */
  /*
    "id" : 1,
            "title" : "Haber 1",
            "sub-title":"Haber 1 Alt Metin",
            "image":"https://images.pexels.com/photos/343219/pexels-photo-343219.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb",
            "writer":"Haber 1 Yazarı"
     */
public class RetrofitNews {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("sub-title")
    private String subtitle;

    @SerializedName("image")
    private String image;

    @SerializedName("writer")
    private String writer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }
}
