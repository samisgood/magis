package com.samir.swipedynview.tab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.app.ActionBar.LayoutParams;
import android.widget.Toast;

import com.google.gson.Gson;
import com.samir.swipedynview.R;
import com.samir.swipedynview.Util;
import com.samir.swipedynview.model.ModelRandom;

public class FragOne extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);

        final LinearLayout lm_left = (LinearLayout) view.findViewById(R.id.ll_frag_one_left);
        final LinearLayout lm_right = (LinearLayout) view.findViewById(R.id.ll_frag_one_right);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        //  setDynamicView(lm_left,3);
        //  setDynamicView(lm_right,4);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        Gson gson = new Gson();
        String json = prefs.getString(getString(R.string.dynamic_model), "");
        ModelRandom model = gson.fromJson(json, ModelRandom.class);


        if (model.getNum_frag_1_left() != -1) {
            Util.getInstance(getActivity()).setDynamicView(lm_left, model.getNum_frag_1_left(),model.getColor_frag_1_left());
        }
        if (model.getNum_frag_1_right()!= -1) {
            Util.getInstance(getActivity()).setDynamicView(lm_right, model.getNum_frag_1_right(),model.getColor_frag_1_right());
        }


        return view;
    }


    private void setDynamicView(LinearLayout linlay, int limit) {
        for (int j = 1; j <= limit; j++) {

            LinearLayout ll = new LinearLayout(getActivity());
            ll.setOrientation(LinearLayout.HORIZONTAL);
            final Button btn = new Button(getActivity());
            // Give button an ID
            btn.setId(j + 1);
            btn.setText("Add To Cart");
            // set the layoutParams on the button
            // btn.setLayoutParams(params);
            btn.setHeight(350);
            btn.setWidth(500);
            btn.setGravity(Gravity.CENTER);
            final int index = j;
            // Set click listener for button
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Log.i("TAG", "index :" + index);

                    Toast.makeText(getActivity(),
                            "Clicked Button Index :" + index,
                            Toast.LENGTH_LONG).show();
                }
            });

            //Add button to LinearLayout
            ll.addView(btn);
            //Add button to LinearLayout defined in XML
            linlay.addView(ll);

        }
    }
}
