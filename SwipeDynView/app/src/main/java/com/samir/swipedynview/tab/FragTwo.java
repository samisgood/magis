package com.samir.swipedynview.tab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.samir.swipedynview.R;
import com.samir.swipedynview.Util;
import com.samir.swipedynview.model.ModelRandom;

public class FragTwo extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_2, container, false);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        final LinearLayout lm_left = (LinearLayout) view.findViewById(R.id.ll_frag_two_left);
        final LinearLayout lm_right = (LinearLayout) view.findViewById(R.id.ll_frag_two_right);

        Gson gson = new Gson();
        String json = prefs.getString(getString(R.string.dynamic_model), "");
        ModelRandom model = gson.fromJson(json, ModelRandom.class);


        if (model.getNum_frag_2_left() != -1) {
            Util.getInstance(getActivity()).setDynamicView(lm_left, model.getNum_frag_2_left(),model.getColor_frag_2_left());
        }
        if (model.getNum_frag_2_right()!= -1) {
            Util.getInstance(getActivity()).setDynamicView(lm_right, model.getNum_frag_2_right(),model.getColor_frag_2_right());
        }


        return view;
    }
}
