package com.sam.magisss.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Samir on 29.4.2018.
 */

public class ResultData {


    @SerializedName("newsList")
    private List<NewModel> newModels;

    public List<NewModel> getNewModels() {
        return newModels;
    }

    public void setNewModels(List<NewModel> newModels) {
        this.newModels = newModels;
    }
}
