package com.sam.magisss.service;

import com.sam.magisss.model.NewModel;
import com.sam.magisss.model.ResultM;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;


/**
 * Created by Samir on 29.4.2018.
 */

public interface APPService {

    /*@GET("json")
    Call<RModel> getNewsDetail();

    * */
    @GET("getNewsDetail")
    Call<RModel> getNewsDetail(@Query("id") int id);


    @FormUrlEncoded
    @POST("saveReadDetail")
    Call<ReadModel> saveReadNewDetails(@Field("id") int id);




}
