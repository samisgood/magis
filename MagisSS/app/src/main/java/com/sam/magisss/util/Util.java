package com.sam.magisss.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * Created by Samir on 29.4.2018.
 */

public class Util {

    private static Util INSTANCE;
    private Context context;

    public Util(Context context) {
        this.context = context;
    }

    public static synchronized Util getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Util(context);
        }
        return INSTANCE;
    }


    public void getFullScreenTransparancy(Activity activity) {
        if (Build.VERSION.SDK_INT >= 19) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public boolean checkGeneralInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null) && (networkInfo.isConnected());
    }


}
