package com.samir.swipedynview.swipe;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class PreferenceManager {

    private Context context;
    private SharedPreferences shp;

    public PreferenceManager(Context context) {
        this.context = context;
        getSharedPreference();
    }

    private void getSharedPreference() {
        shp = context.getSharedPreferences("sam", MODE_PRIVATE);
    }

    public void writePreference() {
        SharedPreferences.Editor editor = shp.edit();
        editor.putString("key", "INIT_OK");
        editor.commit();
    }

    // ilk defa çalıştırma durumu
    public boolean checkPreference() {
        boolean status = false;
        if (shp.getString("key", "null").equals("null")) {
            status = false; // boş
        } else {
            status = true;
        }
        return status;
    }

    public void clearPreference() {
        shp.edit().clear().commit();
    }

}
