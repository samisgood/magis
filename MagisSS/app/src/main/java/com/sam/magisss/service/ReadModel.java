package com.sam.magisss.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Samir on 29.4.2018.
 */
  /*
 "error": false,
    "response": "SUCCESS"
     */
public class ReadModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Boolean error;

    @SerializedName("response")
    @Expose
    private String response;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
