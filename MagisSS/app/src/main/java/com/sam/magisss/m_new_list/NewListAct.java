package com.sam.magisss.m_new_list;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sam.magisss.R;
import com.sam.magisss.m_about.AboutAct;
import com.sam.magisss.m_detail.DetailActivity;
import com.sam.magisss.model.NewModel;
import com.sam.magisss.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NewListAct extends AppCompatActivity {

    private final String URL_TO_HIT = "http://demo5362749.mockable.io/getAllNews";

    private CardView mCv_about;
    private ListView mListView;
    private ProgressDialog dialog;

    private SharedPreferences shp;
    private SharedPreferences.Editor editor;

    private AlertDialog.Builder alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.getInstance(NewListAct.this).getFullScreenTransparancy(NewListAct.this);
        setContentView(R.layout.act_new_list);

        mCv_about = (CardView) findViewById(R.id.cardview_act_new_list_about);
        mListView = (ListView) findViewById(R.id.listview_act_newlist);


        shp = getSharedPreferences("sam", MODE_PRIVATE);
        editor = shp.edit();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("Loading. Please wait...");


        boolean firstRun = shp.getBoolean("isFirstRun", true);
        alertDialog = new AlertDialog.Builder(NewListAct.this);

        if (firstRun) {
            //todo pop up
            new CountDownTimer(2000, 1000) {

                public void onTick(long millisUntilFinished) {
                    dialog.setMessage("Hoşgeldiniz");
                    dialog.show();
                }

                public void onFinish() {
                    dialog.dismiss();
                }

            }.start();
            editor.putBoolean("isFirstRun", false).commit();
        }

        dialog.setMessage("Loading. Please wait...");

        // Create default options which will be used for every
        //  displayImage(...) call if no options will be passed to this method
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config); // Do it on Application start

        new JSONTask().execute(URL_TO_HIT);


        mCv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(NewListAct.this, AboutAct.class);
                //intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                // finish();

            }
        });

    }

    //*****************************AsyncTask*******************************
    public class JSONTask extends AsyncTask<String, String, List<NewModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected List<NewModel> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONObject parentArray = parentObject.getJSONObject("response");
                // JSONArray parentArray = parentObject.getJSONArray("response");
                JSONArray parentArray2 = parentArray.getJSONArray("newsList");

                List<NewModel> movieModelList = new ArrayList<>();

                Gson gson = new Gson();
                for (int i = 0; i < parentArray2.length(); i++) {
                    JSONObject finalObject = parentArray2.getJSONObject(i);
                    /**
                     * below single line of code from Gson saves you from writing the json parsing yourself
                     * which is commented below
                     */
                    NewModel movieModel = gson.fromJson(finalObject.toString(), NewModel.class); // a single line json parsing using Gson
                    movieModel.setId(finalObject.getInt("id"));
                    movieModel.setTitle(finalObject.getString("title"));
                    movieModel.setSubtitle(finalObject.getString("sub-title"));
                    movieModel.setImage(finalObject.getString("image"));
                    movieModel.setWriter(finalObject.getString("writer"));
//                    movieModel.setMovie(finalObject.getString("movie"));
//                    movieModel.setYear(finalObject.getInt("year"));
//                    movieModel.setRating((float) finalObject.getDouble("rating"));
//                    movieModel.setDirector(finalObject.getString("director"));
//
//                    movieModel.setDuration(finalObject.getString("duration"));
//                    movieModel.setTagline(finalObject.getString("tagline"));
//                    movieModel.setImage(finalObject.getString("image"));
//                    movieModel.setStory(finalObject.getString("story"));
//
//                    List<MovieModel.Cast> castList = new ArrayList<>();
//                    for(int j=0; j<finalObject.getJSONArray("cast").length(); j++){
//                        MovieModel.Cast cast = new MovieModel.Cast();
//                        cast.setName(finalObject.getJSONArray("cast").getJSONObject(j).getString("name"));
//                        castList.add(cast);
//                    }
//                    movieModel.setCastList(castList);
                    // adding the final object in the list
                    movieModelList.add(movieModel);
                }
                return movieModelList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<NewModel> result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result != null) {
                MovieAdapter adapter = new MovieAdapter(getApplicationContext(), R.layout.item_act_new_list, result);
                mListView.setAdapter(adapter);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {  // list item click opens a new detailed activity
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        NewModel movieModel = result.get(position); // getting the model

                        Gson gson = new Gson();
                        String json = gson.toJson(movieModel);
                        editor.putString("news", json);
                        editor.commit();

                        Intent intent = new Intent(NewListAct.this, DetailActivity.class);
                        intent.putExtra("movieModel", new Gson().toJson(movieModel)); // converting model json into string type and sending it via intent
                        intent.putExtra("mage_id", movieModel.getId()); // converting model json into string type and sending it via intent
                        startActivity(intent);

                    }
                });
            } else {
                Toast.makeText(getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //*****************************AsyncTask*******************************

    //*****************************Adaptor*********************************
    public class MovieAdapter extends ArrayAdapter {

        private List<NewModel> newsList;
        private int resource;
        private LayoutInflater inflater;

        public MovieAdapter(Context context, int resource, List<NewModel> objects) {
            super(context, resource, objects);
            newsList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.newImage = (ImageView) convertView.findViewById(R.id.ivIcon);
                holder.newTitle = (TextView) convertView.findViewById(R.id.item_act_new_lits_new_title);
                holder.newSubTitle = (TextView) convertView.findViewById(R.id.item_act_new_lits_new_sub_title);
                holder.newWriter = (TextView) convertView.findViewById(R.id.item_act_new_lits_new_writer);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final ViewHolder finalHolder = holder;
            ImageLoader.getInstance().displayImage(newsList.get(position).getImage(), holder.newImage, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    finalHolder.newImage.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                    finalHolder.newImage.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);
                    finalHolder.newImage.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);
                    finalHolder.newImage.setVisibility(View.INVISIBLE);
                }
            });

            holder.newTitle.setText("" + newsList.get(position).getTitle());
            holder.newSubTitle.setText("" + newsList.get(position).getSubtitle());
            holder.newWriter.setText("" + newsList.get(position).getWriter());


            return convertView;
        }


        class ViewHolder {
            private ImageView newImage;
            private TextView newTitle, newSubTitle, newWriter;

        }

    }
    //*********************************************************************
}
