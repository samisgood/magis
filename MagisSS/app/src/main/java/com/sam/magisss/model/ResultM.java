package com.sam.magisss.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samir on 29.4.2018.
 */

public class ResultM {

    @SerializedName("error")
    private String error;

    @SerializedName("response")
    private ResultData responseM;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ResultData getResponseM() {
        return responseM;
    }

    public void setResponseM(ResultData responseM) {
        this.responseM = responseM;
    }
}
