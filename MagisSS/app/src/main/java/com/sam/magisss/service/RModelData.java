package com.sam.magisss.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sam.magisss.model.NewModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Samir on 29.4.2018.
 */

public class RModelData implements Serializable {


    @SerializedName("news")
    @Expose
    RModelDataItems newDetails;

    public RModelDataItems getNewDetails() {
        return newDetails;
    }

    public void setNewDetails(RModelDataItems newDetails) {
        this.newDetails = newDetails;
    }
}
