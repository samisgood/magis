package com.samir.swipedynview.swipe

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.samir.swipedynview.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
