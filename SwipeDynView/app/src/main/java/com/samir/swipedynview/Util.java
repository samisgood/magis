package com.samir.swipedynview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.samir.swipedynview.activity.SecondAct;
import com.samir.swipedynview.model.ModelRandom;

import java.util.Random;


public final class Util {

    private static Util INSTANCE;
    private final Context mContext;

    public Util(Context context) {
        this.mContext = context;
    }

    public static synchronized Util getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Util(context);
        }
        return INSTANCE;
    }

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void getFullScreenTransparancy(Activity activity) {
        if (Build.VERSION.SDK_INT >= 19) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public int getRandomValue() {
        Random rn = new Random();
        return rn.nextInt(3) + 1;
    }

    public synchronized int giveMeRandomColor() {
        Random random = new Random();
        return Color.argb(255, random.nextInt(256), random.nextInt(256),
                random.nextInt(256));
    }

    public synchronized void setDynamicView(LinearLayout linlay, int limit, final int[] array) {
        for (int j = 0; j < limit; j++) {

            LinearLayout ll = new LinearLayout(mContext);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            final Button btn = new Button(mContext);
            //btn.setId(j + 1);
          //  btn.setText("" + getRandomValue());
            btn.setHeight(500);
            btn.setWidth(500);
            btn.setGravity(Gravity.CENTER);
            btn.setBackgroundColor(array[j]);

            final int index = j;
            final int currentColor = array[j];
            // Set click listener for button
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, SecondAct.class);
                    intent.putExtra(mContext.getString(R.string.current_color), currentColor);
                    mContext.startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    Log.i("TAG", "index :" + index);

                    //  Toast.makeText(mContext, "Clicked Button Index :" + index, Toast.LENGTH_LONG).show();
                }
            });

            //Add button to LinearLayout
            ll.addView(btn);
            //Add button to LinearLayout defined in XML
            linlay.addView(ll);

        }
    }

    public void setFragmentRandomNumbers() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        ModelRandom model = new ModelRandom();
        model.setNum_frag_1_left(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_1_left(), model.getNum_frag_1_left());

        model.setNum_frag_2_left(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_2_left(), model.getNum_frag_2_left());

        model.setNum_frag_3_left(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_3_left(), model.getNum_frag_3_left());

        model.setNum_frag_1_right(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_1_right(), model.getNum_frag_1_right());

        model.setNum_frag_2_right(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_2_right(), model.getNum_frag_2_right());

        model.setNum_frag_3_right(Util.getInstance(mContext).getRandomValue());
        setArrayWithRandomColor(model.getColor_frag_3_right(), model.getNum_frag_3_right());

        Gson gson = new Gson();
        String json = gson.toJson(model);
        editor.putString(mContext.getString(R.string.dynamic_model), json);
        editor.commit();
    }

    public void setArrayWithRandomColor(int[] array, int limit) {
        for (int i = 0; i < limit; i++) {
            array[i] = giveMeRandomColor();
        }
    }

}