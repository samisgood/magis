package com.samir.swipedynview.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.samir.swipedynview.R;
import com.samir.swipedynview.Util;

public class SecondAct extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.getInstance(SecondAct.this).getFullScreenTransparancy(SecondAct.this);
        setContentView(R.layout.act_second);

        LinearLayout linlay = (LinearLayout) findViewById(R.id.linlay_act_second);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int color = extras.getInt(getString(R.string.current_color));
            linlay.setBackgroundColor(color);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
