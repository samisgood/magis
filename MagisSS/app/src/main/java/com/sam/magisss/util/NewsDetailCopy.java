package com.sam.magisss.util;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sam.magisss.R;
import com.sam.magisss.m_about.AboutAct;
import com.sam.magisss.m_new_list.NewListAct;
import com.sam.magisss.model.NewModel;
import com.sam.magisss.service.APPService;
import com.sam.magisss.service.RModel;
import com.sam.magisss.service.RModelDataItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class NewsDetailCopy extends AppCompatActivity {


    private ImageView img_back;
    private CardView mCv_about, mCv_new_list;
    private ListView mListView;
    private ProgressDialog pDialog;
    private Retrofit retrofit;

    private SharedPreferences shp;
    private SharedPreferences.Editor editor;
    private Retrofit restAdapter;
    private APPService restInterface;

    private String URL_OF_NEW;
    private String urlJsonObj = "https://api.androidhive.info/volley/person_object.json";
    private String jsonResponse;
    private static String TAG = NewsDetailCopy.class.getSimpleName();
    private RModelDataItems currentNew;

    private TextView txt_title, txt_sub_title, txt_description;
    private ImageView img_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.getInstance(NewsDetailCopy.this).getFullScreenTransparancy(NewsDetailCopy.this);
        setContentView(R.layout.act_details);


        img_back = (ImageView) findViewById(R.id.img_act_details_back);
        mCv_new_list = (CardView) findViewById(R.id.cardview_act_detail_new_list);
        mCv_about = (CardView) findViewById(R.id.cardview_act_detail_new_about);

        txt_title = (TextView) findViewById(R.id.txt_act_detals_title);
        txt_sub_title = (TextView) findViewById(R.id.txt_act_detals_sub_title);
        txt_description = (TextView) findViewById(R.id.txt_act_detals_description);
        img_ = (ImageView) findViewById(R.id.img_act_details_image);

        shp = getSharedPreferences("sam", MODE_PRIVATE);
        editor = shp.edit();
        currentNew = null;

        pDialog = new ProgressDialog(NewsDetailCopy.this);
        pDialog.setMessage(getString(R.string.dialog_msg));
        pDialog.setCancelable(false);


        // adapter ve servis oluşturma

        try {
            Gson gsonn = new Gson();
            String jsonn = shp.getString("news", "");
            NewModel modell = gsonn.fromJson(jsonn, NewModel.class);
            String samm = "";

            if (modell != null) {
                URL_OF_NEW = modell.getImage();
                getAndSetImageDetails(URL_OF_NEW, modell.getId());


                // makeJsonObjectRequest();
                //new JSONTask().execute(URL_OF_NEW);

              /*
              *   String url = URL_OF_NEW;
                //  String url = "http://api.giphy.com/v1/gifs/search?q=funny+cat&api_key=dc6zaTOxFJmzC";
                StringRequest stringRequest = new StringRequest(url, new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            String body = response.toString();
                            String sam = "";
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonArray = jsonObject.getJSONObject("response");

                        } catch (JSONException ee){
                            ee.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Anything you want
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
              * */


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // volley try


        mCv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(NewsDetailCopy.this, AboutAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        mCv_new_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(NewsDetailCopy.this, NewListAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(NewsDetailCopy.this, NewListAct.class);
                intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

    }

    private void setViewDetails(RModelDataItems modell) {
        if (currentNew != null) {
            txt_title.setText(modell.getTitle());
            txt_sub_title.setText(modell.getSubtitle());
            txt_description.setText(modell.getTitle());

            //****************************************************************
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar_act_details);
            ImageLoader.getInstance().displayImage(modell.getImage(), img_, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    img_.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);
                    img_.setVisibility(View.INVISIBLE);
                }
            });
            //****************************************************************


        }
    }

    //*****************************************************************Volley
    private void makeJsonObjectRequest() {

        //showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL_OF_NEW, null, new com.android.volley.Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {

                    /*
    {
  "error": false,
  "response": {
    "news":
      {
        "id": 1,
        "title": "Haber 1",
        "sub-title": "Haber 1 Alt Metin",
        "image": "https://images.pexels.com/photos/343219/pexels-photo-343219.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb",
        "writer": "Haber 1 Yazarı",
        "description":"Haber 1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      }
  }
}
	*/
                    boolean error = response.getBoolean("error");
                    JSONObject body = response.getJSONObject("response");
                    int id = body.getInt("id");
                    String title = body.getString("title");
                    String sub = body.getString("sub-title");
                    String image = body.getString("image");
                    String description = body.getString("description");

                    jsonResponse = "";
                    jsonResponse += "Name: " + id + "\n\n";
                    jsonResponse += "Email: " + title + "\n\n";
                    jsonResponse += "Home: " + sub + "\n\n";
                    jsonResponse += "Mobile: " + description + "\n\n";

                    //  txtResponse.setText(jsonResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                //    hidepDialog();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                //  hidepDialog();
            }
        });

        // Adding request to request queue
        //    AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
    //*****************************************************************Volley


    //*****************************AsyncTask*******************************
    public class JSONTask extends AsyncTask<String, String, List<RModelDataItems>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();
        }

        @Override
        protected List<RModelDataItems> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream, "iso-8859-1"), 8);
                //BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);

                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();


                JSONObject parentObject = new JSONObject(finalJson);
                JSONObject resonseObject = parentObject.getJSONObject("response");
                JSONObject newsObject = resonseObject.getJSONObject("news");

                JSONArray parentArray2 = resonseObject.getJSONArray("response");

                List<RModelDataItems> movieModelList = new ArrayList<>();

                Gson gson = new Gson();

                for (int i = 0; i < parentArray2.length(); i++) {
                    JSONObject finalObject = parentArray2.getJSONObject(i);


                    RModelDataItems movieModel = gson.fromJson(newsObject.toString(), RModelDataItems.class);
                    movieModel.setId(newsObject.getInt("id"));
                    movieModel.setTitle(newsObject.getString("title"));
                    movieModel.setSubtitle(newsObject.getString("sub-title"));
                    movieModel.setImage(newsObject.getString("image"));
                    movieModel.setWriter(newsObject.getString("writer"));
                    movieModel.setDescription(newsObject.getString("description"));
                    /**
                     *
                     * below single line of code from Gson saves you from writing the json parsing yourself
                     * which is commented below
                     *
                     *   NewModel movieModel = gson.fromJson(finalObject.toString(), NewModel.class); // a single line json parsing using Gson
                     movieModel.setId(finalObject.getInt("id"));
                     movieModel.setTitle(finalObject.getString("title"));
                     movieModel.setSubtitle(finalObject.getString("sub-title"));
                     movieModel.setImage(finalObject.getString("image"));
                     movieModel.setWriter(finalObject.getString("writer"));
                     */

//                    movieModel.setMovie(finalObject.getString("movie"));
//                    movieModel.setYear(finalObject.getInt("year"));
//                    movieModel.setRating((float) finalObject.getDouble("rating"));
//                    movieModel.setDirector(finalObject.getString("director"));
//
//                    movieModel.setDuration(finalObject.getString("duration"));
//                    movieModel.setTagline(finalObject.getString("tagline"));
//                    movieModel.setImage(finalObject.getString("image"));
//                    movieModel.setStory(finalObject.getString("story"));
//
//                    List<MovieModel.Cast> castList = new ArrayList<>();
//                    for(int j=0; j<finalObject.getJSONArray("cast").length(); j++){
//                        MovieModel.Cast cast = new MovieModel.Cast();
//                        cast.setName(finalObject.getJSONArray("cast").getJSONObject(j).getString("name"));
//                        castList.add(cast);
//                    }
//                    movieModel.setCastList(castList);
                    // adding the final object in the list
                    movieModelList.add(movieModel);
                }
                return movieModelList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<RModelDataItems> result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (result != null) {
                String sam = "";
            } else {
                Toast.makeText(getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //*****************************AsyncTask*******************************


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            try {
                pDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void getHeros() {

    }

    private void getAndSetImageDetails(String url, int id) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo5362749.mockable.io/").
                        addConverterFactory(GsonConverterFactory.create())
                .build();
        APPService service = retrofit.create(APPService.class);

        final Call<RModel> reportMCall = service.getNewsDetail(id);
        reportMCall.enqueue(new Callback<RModel>() {
            @Override
            public void onResponse(Response<RModel> response, Retrofit retrofit) {
                hidepDialog();

                RModel datam = response.body();
                if (datam != null) {
                    boolean status = response.body().isError();

                    if (!status) {
                      //  Util.showMessage(DetailActivity.this, "Oldu");

                        // todo adaptor
                        currentNew = datam.getResponseM().getNewDetails();
                        setViewDetails(currentNew);
                    } else {
                        Util.showMessage(NewsDetailCopy.this, "News Detail Error");
                        Log.d("onResponse", "" + response.code() +
                                "  response body " + response.body() +
                                " responseError " + response.errorBody() + " responseMessage " +
                                response.message());
                    }
                }


            }

            @Override
            public void onFailure(Throwable t) {
                hidepDialog();
                Util.showMessage(NewsDetailCopy.this, "News Detail Connection Error");
                Log.d("onFailure", t.toString());
            }
        });


    }


}
