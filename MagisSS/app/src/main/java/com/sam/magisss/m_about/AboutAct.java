package com.sam.magisss.m_about;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.sam.magisss.R;
import com.sam.magisss.m_new_list.NewListAct;
import com.sam.magisss.util.Util;

public class AboutAct extends AppCompatActivity {

    private CardView mCv_new_list;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.getInstance(AboutAct.this).getFullScreenTransparancy(AboutAct.this);
        setContentView(R.layout.act_about);

        mCv_new_list = (CardView) findViewById(R.id.cardview_act_about_new_list);
        webView = (WebView) findViewById(R.id.webView_act_about);

        if (Util.getInstance(AboutAct.this).checkGeneralInternet()) {
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            WebViewClientImpl webViewClient = new WebViewClientImpl(this);
            webView.setWebViewClient(webViewClient);
            webView.loadUrl("http://www.magis.com.tr");
        }



        mCv_new_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToYoklamaAl = new Intent(AboutAct.this, NewListAct.class);
                //intentToYoklamaAl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToYoklamaAl);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                // finish();

            }
        });
    }
}
