package com.samir.swipedynview.model;

import java.io.Serializable;

public class ModelRandom implements Serializable {

    private int num_frag_1_left;
    private int[] color_frag_1_left;

    private int num_frag_1_right;
    private int[] color_frag_1_right;

    private int num_frag_2_left;
    private int[] color_frag_2_left;

    private int num_frag_2_right;
    private int[] color_frag_2_right;

    private int num_frag_3_left;
    private int[] color_frag_3_left;

    private int num_frag_3_right;
    private int[] color_frag_3_right;

    public ModelRandom(   ) {
        this.color_frag_1_left = new int[3];
        this.color_frag_1_right = new int[3];
        this.color_frag_2_left = new int[3];
        this.color_frag_2_right = new int[3];
        this.color_frag_3_left = new int[3];
        this.color_frag_3_right = new int[3];
    }

    public int getNum_frag_1_left() {
        return num_frag_1_left;
    }

    public void setNum_frag_1_left(int num_frag_1_left) {
        this.num_frag_1_left = num_frag_1_left;
    }

    public int[] getColor_frag_1_left() {
        return color_frag_1_left;
    }

    public void setColor_frag_1_left(int[] color_frag_1_left) {
        this.color_frag_1_left = color_frag_1_left;
    }

    public int getNum_frag_1_right() {
        return num_frag_1_right;
    }

    public void setNum_frag_1_right(int num_frag_1_right) {
        this.num_frag_1_right = num_frag_1_right;
    }

    public int[] getColor_frag_1_right() {
        return color_frag_1_right;
    }

    public void setColor_frag_1_right(int[] color_frag_1_right) {
        this.color_frag_1_right = color_frag_1_right;
    }

    public int getNum_frag_2_left() {
        return num_frag_2_left;
    }

    public void setNum_frag_2_left(int num_frag_2_left) {
        this.num_frag_2_left = num_frag_2_left;
    }

    public int[] getColor_frag_2_left() {
        return color_frag_2_left;
    }

    public void setColor_frag_2_left(int[] color_frag_2_left) {
        this.color_frag_2_left = color_frag_2_left;
    }

    public int getNum_frag_2_right() {
        return num_frag_2_right;
    }

    public void setNum_frag_2_right(int num_frag_2_right) {
        this.num_frag_2_right = num_frag_2_right;
    }

    public int[] getColor_frag_2_right() {
        return color_frag_2_right;
    }

    public void setColor_frag_2_right(int[] color_frag_2_right) {
        this.color_frag_2_right = color_frag_2_right;
    }

    public int getNum_frag_3_left() {
        return num_frag_3_left;
    }

    public void setNum_frag_3_left(int num_frag_3_left) {
        this.num_frag_3_left = num_frag_3_left;
    }

    public int[] getColor_frag_3_left() {
        return color_frag_3_left;
    }

    public void setColor_frag_3_left(int[] color_frag_3_left) {
        this.color_frag_3_left = color_frag_3_left;
    }

    public int getNum_frag_3_right() {
        return num_frag_3_right;
    }

    public void setNum_frag_3_right(int num_frag_3_right) {
        this.num_frag_3_right = num_frag_3_right;
    }

    public int[] getColor_frag_3_right() {
        return color_frag_3_right;
    }

    public void setColor_frag_3_right(int[] color_frag_3_right) {
        this.color_frag_3_right = color_frag_3_right;
    }
}
